Horse Track Programming Assignment
  ---

This is an implementation of the [Horse Track Programming Assignment](Horse%20Track%20Programming%20Assignment.pdf) . 


## REQUIREMENTS
Java >= 1.8, Scala 2.13, sbt 1.3.8

## BUILD AND RUN
1. In a terminal:


        cd <project-home>
        sbt clean compile
 	    sbt run
 	    sbt test

This repository can also be imported into an IDE such as Intellij with [SBT Plugins support](https://plugins.jetbrains.com/plugin/5007-sbt) .

## NOTES
I found [this implementation of the Horse Track Programming Assignment](https://github.com/artisancreek/horsetrack) when I was researching this project - so I didn't want to implement my solution in the same way.  

I have taken the Java model packages from the above repository and used them within a Scala sbt solution.  The idea is to use Java objects inside of a solution implemented using common functional programming design patterns, and to compare how this was implemented in a more OO approach using Java and Spring.  

It has been fun exploring these ideas in code.  Functional code can be faster, I have not benchmarked this against the Java Spring implementation.  Functional code is also faster to write, easier to maintain, and also well suited to being used in a streaming context.

