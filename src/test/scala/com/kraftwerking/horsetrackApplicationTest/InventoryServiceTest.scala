package com.kraftwerking.horsetrackApplicationTest

import com.kraftwerking.horsetrackApplication.model.{Horse, Inventory, RaceStatus}
import com.kraftwerking.horsetrackApplication.service._
import org.scalatest.FunSuite

class InventoryServiceTest extends FunSuite {
  test("InventoryService is able to load 5 Inventories") {
    val inventory = InventoryService.loadInventory()
    assert(inventory.length == 5)
  }

  test("Inventory calculatePayout should print invalid bet if bet amount is wrong format") {
    val inventory = InventoryService.loadInventory()
    val results: Either[String, Array[Inventory]] = InventoryService.calculatePayout(new Horse(1, "That Darn Gray Cat", 5, RaceStatus.WON), "44.44", inventory)
    results match {
      case Left(s) =>
        assert(s == "Invalid Bet: 44.44")
      case Right(a) =>
    }
  }

  test("Inventory getInventoryTotal should return correct amount") {
    val inventory = InventoryService.loadInventory()
    val results = InventoryService.getInventoryTotal(inventory)
    assert(results == 1360)
  }

  test("Inventory calculatePayout should return updated inventory") {
    val inventory = InventoryService.loadInventory()
    val results: Either[String, Array[Inventory]] = InventoryService.calculatePayout(new Horse(1, "That Darn Gray Cat", 5, RaceStatus.WON), "55", inventory)
    results match {
      case Left(s) =>
      case Right(a) =>
        assert(a sameElements inventory)
    }
  }

  test("Inventory calculatePayout should return insufficient funds if not enough money in inventory") {
    val inventory = Array(new Inventory(1, 10), new Inventory(5, 5), new Inventory(10, 5),
      new Inventory(20, 1), new Inventory(100, 0))
    val results: Either[String, Array[Inventory]] = InventoryService.calculatePayout(new Horse(1, "That Darn Gray Cat", 5, RaceStatus.WON), "55", inventory)
    results match {
      case Left(s) =>
        assert(s == "Insufficient Funds: 275")
      case Right(a) =>
    }
  }

  test("Inventory calculatePayout should print insufficient funds is winnings is greater than total inventory") {
    val inventory = InventoryService.loadInventory()
    val results: Either[String, Array[Inventory]] = InventoryService.calculatePayout(new Horse(1, "That Darn Gray Cat", 5, RaceStatus.WON), "4444", inventory)
    results match {
      case Left(s) =>
        assert(s == "Insufficient Funds: " + (4444 * 5))
      case Right(a) =>
    }
  }
}