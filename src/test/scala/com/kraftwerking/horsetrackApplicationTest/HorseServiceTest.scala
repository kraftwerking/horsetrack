package com.kraftwerking.horsetrackApplicationTest

import com.kraftwerking.horsetrackApplication.model.{Horse, RaceStatus}
import com.kraftwerking.horsetrackApplication.service._
import org.scalatest.FunSuite

class HorseServiceTest extends FunSuite {
  test("HorseService is able to load 7 Horses") {
    val horses = HorseService.loadHorses()
    assert(horses.length == 7)
  }

  test("HorseService is able to set winning horse") {
    val horses = HorseService.loadHorses()
    val results: Either[String, Array[Horse]] = HorseService.setWinningHorse("4", horses)
    results match {
      case Left(s) =>
      case Right(a) =>
        assert(a.length == 7)
        var result = false
        val horseArr = a.filter(x => x.getHorseNumber == 4 && x.getRaceStatus == RaceStatus.WON)
        if (horseArr.length == 1) result = true
        assert(result)
    }
  }

  test("HorseService set winning horse prints error when winning horse does not exist") {
    val horses = HorseService.loadHorses()
    val results: Either[String, Array[Horse]] = HorseService.setWinningHorse("8", horses)
    results match {
      case Left(s) =>
        assert(s.length > 0)
        assert(s == "Invalid Horse Number: 8")
      case Right(a) =>
    }
  }

  test("HorseService gets valid horse") {
    val horses = HorseService.loadHorses()
    HorseService.getValidHorse("4", horses) match {
      case Some(i) =>
        assert(i.getHorseNumber == 4)
      case None =>
    }
  }

  test("HorseService returns None when not valid horse") {
    val horses = HorseService.loadHorses()
    val horse = HorseService.getValidHorse("8", horses)
    assert(horse.isEmpty)
  }

  test("HorseService gets winning horse") {
    val horses = HorseService.loadHorses()
    HorseService.getWinningHorse(horses) match {
      case Some(i) =>
        assert(i.getHorseNumber == 1)
        assert(i.getRaceStatus == RaceStatus.WON)
      case None =>
    }
  }

  test("HorseService wager returns winning horse when winning horse number") {
    val horses = HorseService.loadHorses()
    val results: Either[String, Horse] = HorseService.wager("1", horses)
    results match {
      case Left(s) =>
      case Right(h) =>
        assert(h.getHorseNumber == 1)
        assert(h.getRaceStatus == RaceStatus.WON)
    }
  }

  test("HorseService wager returns no payout when not winning horse number") {
    val horses = HorseService.loadHorses()
    val results: Either[String, Horse] = HorseService.wager("4", horses)
    results match {
      case Left(s) =>
        assert(s == "No Payout: Ms Traitour")
      case Right(h) =>
    }
  }

}