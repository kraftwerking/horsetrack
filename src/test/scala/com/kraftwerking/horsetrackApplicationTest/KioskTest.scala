package com.kraftwerking.horsetrackApplicationTest

import com.kraftwerking.horsetrackApplication.controller.Kiosk._
import org.scalatest.FunSuite

class KioskTest extends FunSuite {
  test("parseCommand should interpret command correctly") {
    var command = parseCommand("q")
    assert(command.length == 1)
    assert(command(0) == "quit")

    command = parseCommand("r")
    assert(command.length == 1)
    assert(command(0) == "restock")

    command = parseCommand("W 4")
    assert(command.length == 2)
    assert(command(0) == "winner")
    assert(command(1) == "4")

    command = parseCommand("1 55")
    assert(command.length == 3)
    assert(command(0) == "wager")
    assert(command(1) == "1")
    assert(command(2) == "55")

    command = parseCommand("eeeee")
    assert(command.length == 1)
    assert(command(0) == "invalid")
  }

}