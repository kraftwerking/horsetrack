package com.kraftwerking.horsetrackApplication.controller

trait HorsetrackAccessor {
  def run(): Unit

  def parseCommand(cmd: String): Array[String]
}
