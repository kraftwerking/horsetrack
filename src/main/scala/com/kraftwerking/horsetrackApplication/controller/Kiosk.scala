package com.kraftwerking.horsetrackApplication.controller

import com.kraftwerking.horsetrackApplication.model.{Horse, Inventory}
import com.kraftwerking.horsetrackApplication.service.{HorseService, InventoryService}

import scala.io.StdIn.readLine

object Kiosk extends HorsetrackAccessor {

  def run(): Unit = {
    Console.println("Kiosk Initializing ...")
    var inventory = InventoryService.loadInventory()
    var horses = HorseService.loadHorses()

    //main loop
    var cmd = ""
    while (cmd != "Q" && cmd != "q") {
      InventoryService.printInventory(inventory)
      HorseService.printHorses(horses)
      cmd = readLine("").trim()
      if (cmd.length() > 0) {
        println("Command issued: " + cmd)
        val command = parseCommand(cmd)
        command(0).toLowerCase match {
          case "invalid" => println("Invalid command: " + cmd)
          case "restock" => inventory = InventoryService.loadInventory()
          case "winner" =>
            val results: Either[String, Array[Horse]] = HorseService.setWinningHorse(command(1), horses)
            results match {
              case Left(s) => println(s)
              case Right(a) => horses = a
            }
          case "wager" =>
            val results: Either[String, Horse] = HorseService.wager(command(1), horses)
            results match {
              case Left(s) => println(s)
              case Right(h) =>
                //successful bet, calculate payout
                val res: Either[String, Array[Inventory]] = InventoryService.calculatePayout(h, command(2), inventory)
                res match {
                  case Left(s) => println(s)
                  case Right(i) => inventory = i
                }
            }
          case _ =>
        }
      }
    }
  }

  def parseCommand(cmd: String): Array[String] = {
    val cmdArr = cmd.split(" ")
    cmdArr(0).toLowerCase match {
      case "q" => Array("quit")
      case "r" => Array("restock")
      case "w" if cmd.matches("[W,w] [1-9]") => Array("winner", cmdArr(1))
      case _ if cmd.matches("[0-9]+ [0-9]+.?[0-9]*") => Array("wager", cmdArr(0), cmdArr(1))
      case _ => Array("invalid")
    }
  }

}
