package com.kraftwerking.horsetrackApplication.service

import com.kraftwerking.horsetrackApplication.model.{Horse, RaceStatus}

object HorseService {
  def loadHorses(): Array[Horse] = {
    Array(new Horse(1, "That Darn Gray Cat", 5, RaceStatus.WON),
      new Horse(2, "Fort Utopia", 10, RaceStatus.LOST), new Horse(3, "Count Sheep", 9, RaceStatus.LOST),
      new Horse(4, "Ms Traitour", 4, RaceStatus.LOST), new Horse(5, "Real Princess", 3, RaceStatus.LOST),
      new Horse(6, "Pa Kettle", 5, RaceStatus.LOST), new Horse(7, "Gin Stinger", 6, RaceStatus.LOST))
  }

  def printHorses(horses: Array[Horse]): Unit = {
    println("Horses:")
    for (i <- horses) {
      println(i.getHorseNumber + "," + i.getHorseName + "," + i.getOdds + "," + i.getRaceStatus.toString.toLowerCase())
    }
  }

  def setWinningHorse(horseNum: String, horses: Array[Horse]): Either[String, Array[Horse]] = {
    val horseArr = horses.filter(x => x.getHorseNumber.toString == horseNum)
    if (!horseArr.isEmpty) {
      for (i <- horses) {
        if (i.getHorseNumber.toString == horseNum) i.setRaceStatus(RaceStatus.WON)
        else i.setRaceStatus(RaceStatus.LOST)
      }
      Right(horses)
    } else {
      Left("Invalid Horse Number: " + horseNum)
    }
  }

  def getValidHorse(horseNum: String, horses: Array[Horse]): Option[Horse] = {
    val horseArr = horses.filter(x => x.getHorseNumber.toString == horseNum)
    if (!horseArr.isEmpty) {
      Some(horseArr(0))
    } else {
      None
    }
  }

  def getWinningHorse(horses: Array[Horse]): Option[Horse] = {
    val horseArr = horses.filter(x => x.getRaceStatus == RaceStatus.WON)
    if (!horseArr.isEmpty) {
      Some(horseArr(0))
    } else {
      None
    }
  }

  def wager(horseNum: String, horses: Array[Horse]): Either[String, Horse] = {
    val validHorse: Option[Horse] = HorseService.getValidHorse(horseNum, horses)
    if (validHorse.isDefined) {
      val winningHorse: Option[Horse] = HorseService.getWinningHorse(horses)
      if (winningHorse.isDefined) {
        if (winningHorse.get.getHorseNumber == validHorse.get.getHorseNumber) {
          Right(winningHorse.get)
        } else {
          Left("No Payout: " + validHorse.get.getHorseName)
        }
      } else {
        Left("Unable to get winning horse")
      }
    } else {
      Left("Invalid Horse Number: " + horseNum)
    }
  }

}
