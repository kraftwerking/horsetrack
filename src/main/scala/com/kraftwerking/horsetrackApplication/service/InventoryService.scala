package com.kraftwerking.horsetrackApplication.service

import com.kraftwerking.horsetrackApplication.model.{Horse, Inventory}

object InventoryService {
  def loadInventory(): Array[Inventory] = {
    Array(new Inventory(1, 10), new Inventory(5, 10), new Inventory(10, 10),
      new Inventory(20, 10), new Inventory(100, 10))
  }

  def printInventory(inventory: Array[Inventory]): Unit = {
    println("Inventory:")
    for (i <- inventory) {
      println("$" + i.getDenomination() + "," + i.getBillCount())
    }
  }

  def getInventoryTotal(inventory: Array[Inventory]): Int = {
    var total = 0
    for (i <- inventory) {
      total += i.getDenomination * i.getBillCount
    }
    total
  }

  def calculatePayout(horse: Horse, bet: String, inventory: Array[Inventory]): Either[String, Array[Inventory]] = {
    if (bet forall Character.isDigit) {
      val totalWinnings = bet.toInt * horse.getOdds
      var updateInvArr: Array[Inventory] = Array.empty[Inventory]
      var dispensingArr: Array[String] = Array.empty[String]

      //update inventory and calculate payout
      if (totalWinnings <= getInventoryTotal(inventory)) {
        var winnings = totalWinnings
        var denoms: Array[Int] = Array.empty[Int]
        for (i <- inventory) {
          denoms = denoms :+ i.getDenomination
        }
        denoms = denoms.reverse

        for (denom <- denoms) {
          val quotient = winnings / denom
          dispensingArr = dispensingArr :+ "$" + denom + "," + quotient
          winnings = winnings - (denom * quotient)
          val tmpInv: Option[Inventory] = decrementInventory(denom, quotient, inventory)
          if (!tmpInv.isEmpty) {
            updateInvArr = updateInvArr :+ tmpInv.get
          }
        }
        if (updateInvArr.length > 0) {
          for (i <- updateInvArr) {
            val invArr = inventory.filter(x => x.getDenomination == i.getDenomination)
            invArr(0).setBillCount(i.getBillCount)
          }
          println("Payout: " + horse.getHorseName + ",$" + totalWinnings)
          println("Dispensing:")
          println(dispensingArr.reverse.mkString("\n"))
          Right(inventory)
        } else {
          Left("Insufficient Funds: " + totalWinnings)
        }

      } else {
        Left("Insufficient Funds: " + totalWinnings)
      }
    } else {
      Left("Invalid Bet: " + bet)
    }
  }

  def decrementInventory(denomination: Int, amount: Int, inventory: Array[Inventory]): Option[Inventory] = {
    val i = inventory.filter(x => x.getDenomination == denomination)(0)
    val currentBillCount = i.getBillCount
    if ((currentBillCount - amount) >= 0) {
      i.setBillCount(currentBillCount - amount)
      Some(i)
    } else {
      None
    }
  }

}