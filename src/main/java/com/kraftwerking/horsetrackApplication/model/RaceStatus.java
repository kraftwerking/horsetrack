package com.kraftwerking.horsetrackApplication.model;

public enum RaceStatus {
    WON,
    LOST;
}
